package classes_a;

import java.util.ArrayList;

import org.dozer.BeanFactory;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class FactoryClassA implements BeanFactory {

    public Object createBean(Object source, Class<?> sourceClass, String targetBeanId) {
        return new FactoryCreatedClassA(1, "a", 1.0, new ArrayList<String>());
    }
}
