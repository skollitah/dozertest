package classes_a;

import java.util.List;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class FactoryCreatedClassA {

    private Integer intA;

    private String someText;

    private double doubleA;
    private List<String> listString;

    public FactoryCreatedClassA(final Integer intA, final String someText, final double doubleA,
            final List<String> listString) {
        this.intA = intA;
        this.someText = someText;
        this.doubleA = doubleA;
        this.listString = listString;
    }

    public List<String> getListString() {
        return listString;
    }

    public void setListString(List<String> listString) {
        this.listString = listString;
    }

    public double getDoubleA() {
        return doubleA;
    }

    public void setDoubleA(double doubleA) {
        this.doubleA = doubleA;
    }

    public String getSomeText() {
        return someText;
    }

    public void setSomeText(String someText) {
        this.someText = someText;
    }

    public Integer getIntA() {
        return intA;
    }

    public void setIntA(Integer intA) {
        this.intA = intA;
    }
}
