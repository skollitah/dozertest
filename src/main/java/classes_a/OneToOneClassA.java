package classes_a;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class OneToOneClassA {

    private int intA;

    private String stringA;

    public String getStringA() {
        return stringA;
    }

    public void setStringA(String stringA) {
        this.stringA = stringA;
    }

    public int getIntA() {
        return intA;
    }

    public void setIntA(int intA) {
        this.intA = intA;
    }
}
