package classes_a;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class OneToOneClassAWithConstructor {

    private int intA;
    private String stringA;

    public OneToOneClassAWithConstructor(final int intA, final String stringA) {
        this.intA = intA;
        this.stringA = stringA;
    }

    public OneToOneClassAWithConstructor() {
    }

    public String getStringA() {
        return stringA;
    }

    public int getIntA() {
        return intA;
    }
}
