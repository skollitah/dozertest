package classes_a;

/**
 * Created by ljozwiak on 2016-04-29.
 */
public class OneToOneClassC extends OneToOneClassA {

    private int intC;

    public int getIntC() {
        return intC;
    }

    public void setIntC(int intC) {
        this.intC = intC;
    }
}
