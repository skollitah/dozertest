package classes_a;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class OneToTwoClassA {

    private int first;

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }
}
