package classes_a;

/**
 * Created by ljozwiak on 2016-04-29.
 */
public class SubOneToOneA extends SuperOneToOneA {

    int intSub;

    public int getIntSub() {
        return intSub;
    }

    public void setIntSub(int intSub) {
        this.intSub = intSub;
    }
}
