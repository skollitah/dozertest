package classes_a;

/**
 * Created by ljozwiak on 2016-04-29.
 */
public class SuperOneToOneA {

    int intSuper;

    public int getIntSuper() {
        return intSuper;
    }

    public void setIntSuper(int intSuper) {
        this.intSuper = intSuper;
    }
}
