package classes_b;

import java.util.HashSet;

import org.dozer.BeanFactory;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class FactoryClassB implements BeanFactory {

    public Object createBean(Object source, Class<?> sourceClass, String targetBeanId) {
        return new FactoryCreatedClassB(1, "b", 1.0, new HashSet<String>());
    }
}
