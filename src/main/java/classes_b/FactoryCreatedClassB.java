package classes_b;

import java.util.Set;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class FactoryCreatedClassB {

    private int intB;

    private String someText;

    private double doubleB;
    private Set<String> setString;

    public FactoryCreatedClassB(final int intB, final String someText, final double doubleB, final Set<String> setString) {
        this.intB = intB;
        this.someText = someText;
        this.doubleB = doubleB;
        this.setString = setString;
    }

    public Set<String> getSetString() {
        return setString;
    }

    public void setSetString(Set<String> setString) {
        this.setString = setString;
    }

    public int getIntB() {
        return intB;
    }

    public void setIntB(int intB) {
        this.intB = intB;
    }

    public String getSomeText() {
        return someText;
    }

    public void setSomeText(String someText) {
        this.someText = someText;
    }

    public double getDoubleB() {
        return doubleB;
    }

    public void setDoubleB(double doubleB) {
        this.doubleB = doubleB;
    }

}
