package classes_b;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class OneToOneClassB {

    private int intA;

    public int getIntA() {
        return intA;
    }

    public void setIntA(int intA) {
        this.intA = intA;
    }
}
