package classes_b;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class OneToOneClassBWithConstructor {

    private int intA;

    public OneToOneClassBWithConstructor(final int intA) {
        this.intA = intA;
    }

    public OneToOneClassBWithConstructor() {
    }

    public int getIntA() {
        return intA;
    }
}
