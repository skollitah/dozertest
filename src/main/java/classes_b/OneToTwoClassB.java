package classes_b;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class OneToTwoClassB {

    private int first;
    private int second;

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }
}
