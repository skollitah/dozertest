package classes_b;

/**
 * Created by ljozwiak on 2016-04-29.
 */
public class SubOneToOneB extends SuperOneToOneB {

    int intSub;

    public int getIntSub() {
        return intSub;
    }

    public void setIntSub(int intSub) {
        this.intSub = intSub;
    }
}
