package mapper;

import classes_a.SuperOneToOneA;
import classes_b.SuperOneToOneB;
import org.dozer.DozerConverter;
import org.dozer.Mapper;
import org.dozer.MapperAware;

/**
 * Created by ljozwiak on 2016-04-29.
 */
public class CustomInheritanceConverter extends DozerConverter<SuperOneToOneA, SuperOneToOneB> implements MapperAware {

    private Mapper mapper;

    public CustomInheritanceConverter() {
        super(SuperOneToOneA.class, SuperOneToOneB.class);
    }

    public SuperOneToOneB convertTo(final SuperOneToOneA source, SuperOneToOneB destination) {
        if (destination == null) {
            destination = new SuperOneToOneB();
        }

        destination.setIntSuper(source.getIntSuper());

        return destination;

    }

    public SuperOneToOneA convertFrom(final SuperOneToOneB source, SuperOneToOneA destination) {
        if (destination == null) {
            destination = new SuperOneToOneA();
        }

        destination.setIntSuper(source.getIntSuper());

        return destination;
    }

    public void setMapper(final Mapper mapper) {
        this.mapper = mapper;
    }
}
