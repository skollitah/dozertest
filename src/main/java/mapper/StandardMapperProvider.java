package mapper;

import java.util.Arrays;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class StandardMapperProvider {

    private static final String MAPPING_FILE = "dozerMappingFile.xml";

    private StandardMapperProvider() {
    }

    public static Mapper getMapper() {
        return new MapperContainer().mapper;
    }

    private static class MapperContainer {

        private Mapper mapper = new DozerBeanMapper(Arrays.asList(MAPPING_FILE));
    }
}
