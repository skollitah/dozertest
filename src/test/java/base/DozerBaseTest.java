package base;

import mapper.StandardMapperProvider;

import org.dozer.Mapper;
import org.junit.Before;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class DozerBaseTest {

    private Mapper mapper;

    protected Mapper getMapper() {
        return mapper;
    }

    @Before
    public void initMapper() {
        mapper = StandardMapperProvider.getMapper();
    }

}
