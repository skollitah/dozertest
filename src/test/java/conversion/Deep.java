package conversion;

/**
 * Created by ljozwiak on 01/10/2015.
 */
public class Deep {

    private int number;
    private String text;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
