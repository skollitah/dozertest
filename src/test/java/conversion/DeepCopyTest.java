package conversion;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import base.DozerBaseTest;

/**
 * Created by ljozwiak on 01/10/2015.
 */
public class DeepCopyTest extends DozerBaseTest {

    private final static int NUMBER = 10;
    private final static String TEXT = "text";

    @Test
    public void testDeepCopy() {
        // given
        final Deep source = new Deep();
        source.setNumber(NUMBER);
        source.setText(TEXT);

        // when
        final Deep dest = new Deep();
        getMapper().map(source, dest);

        assertThat(source.getNumber()).isEqualTo(dest.getNumber());
        assertThat(source.getText()).isEqualTo(dest.getText());
    }
}
