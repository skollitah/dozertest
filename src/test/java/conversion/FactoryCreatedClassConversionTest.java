package conversion;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.Test;

import base.DozerBaseTest;
import classes_a.FactoryCreatedClassA;
import classes_b.FactoryCreatedClassB;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class FactoryCreatedClassConversionTest extends DozerBaseTest {

    @Test
    public void shouldConvertFactoryClassAtoB() {
        // given
        final FactoryCreatedClassA classA =
                new FactoryCreatedClassA(10, "hello", 10.0, Arrays.asList("hello", "world"));

        // when
        final FactoryCreatedClassB classB = getMapper().map(classA, FactoryCreatedClassB.class);

        // then
        assertThat(classB.getIntB()).isEqualTo(10);
        assertThat(classB.getSomeText()).isEqualTo("hello"); // one way mapping
        assertThat(classB.getDoubleB()).isEqualTo(1.0); // value initiated by factory
        assertThat(classB.getSetString()).containsOnly("hello", "world");
    }

    @Test
    public void shouldConvertFactoryClassBtoA() {
        // given
        final FactoryCreatedClassB classB =
                new FactoryCreatedClassB(10, "hello", 10.0, new HashSet<String>(Arrays.asList("hello", "world")));

        // when
        final FactoryCreatedClassA classA = getMapper().map(classB, FactoryCreatedClassA.class);

        // then
        assertThat(classA.getIntA()).isEqualTo(10);
        assertThat(classA.getSomeText()).isEqualTo("a"); // value initiated by factory
        assertThat(classA.getDoubleA()).isEqualTo(1.0); // value initiated by factory
        assertThat(classA.getListString()).containsOnly("hello", "world");
    }
}
