package conversion;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import base.DozerBaseTest;
import classes_a.OneToOneClassA;
import classes_b.OneToOneClassB;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class OneToOneClassConversionTest extends DozerBaseTest {

    @Test
    public void shouldConvertAToBIgnoringMissingBAttribute() {
        // given
        final OneToOneClassA classA = new OneToOneClassA();
        classA.setIntA(10);
        classA.setStringA("hello");

        // when
        final OneToOneClassB classB = getMapper().map(classA, OneToOneClassB.class);

        // then
        assertThat(classB.getIntA()).isEqualTo(10);
    }

    @Test
    public void shouldConvertBToALeavingDefaultAttributeValue() {
        // given
        final OneToOneClassB classB = new OneToOneClassB();
        classB.setIntA(10);

        // when
        final OneToOneClassA classA = getMapper().map(classB, OneToOneClassA.class);

        // then
        assertThat(classA.getIntA()).isEqualTo(10);
        assertThat(classA.getStringA()).isNull();
    }
}
