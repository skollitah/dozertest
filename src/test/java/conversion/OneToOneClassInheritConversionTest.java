package conversion;

import base.DozerBaseTest;
import classes_a.OneToOneClassC;
import classes_a.SubOneToOneA;
import classes_a.SuperOneToOneA;
import classes_b.OneToOneClassB;
import classes_b.SubOneToOneB;
import classes_b.SuperOneToOneB;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by ljozwiak on 2016-04-29.
 */
public class OneToOneClassInheritConversionTest extends DozerBaseTest {

    @Test
    public void shouldConvertCToBInheritance() {
        // given
        final OneToOneClassC classC = new OneToOneClassC();
        classC.setIntA(10);
        classC.setStringA("hello");

        // when
        final OneToOneClassB classB = getMapper().map(classC, OneToOneClassB.class);

        // then
        assertThat(classB.getIntA()).isEqualTo(10);
    }

    @Test
    public void shouldConvertBToCInheritance() {
        // given
        final OneToOneClassB classB = new OneToOneClassB();
        classB.setIntA(10);

        // when
        final OneToOneClassC classC = getMapper().map(classB, OneToOneClassC.class);

        // then
        assertThat(classB.getIntA()).isEqualTo(10);
    }

    @Test(expected = ClassCastException.class)
    public void shouldConvertSuperToSub() {
        // given
        final SuperOneToOneA classA = new SuperOneToOneA();
        classA.setIntSuper(10);

        // when
        final SubOneToOneB classB = getMapper().map(classA, SubOneToOneB.class);

        // then
        assertThat(classB.getIntSuper()).isEqualTo(10);
    }


    @Test(expected = ClassCastException.class)
    public void shouldConvertSubToSub() {
        // given
        final SubOneToOneA classA = new SubOneToOneA();
        classA.setIntSuper(10);

        // when
        final SubOneToOneB classB = getMapper().map(classA, SubOneToOneB.class);

        // then
        assertThat(classB.getIntSuper()).isEqualTo(10);
    }

    @Test
    public void shouldConvertSuperToSuper() {
        // given
        final SuperOneToOneA classA = new SuperOneToOneA();
        classA.setIntSuper(10);

        // when
        final SuperOneToOneB classB = getMapper().map(classA, SubOneToOneB.class);

        // then
        assertThat(classB.getIntSuper()).isEqualTo(10);
    }


    @Test
    public void shouldConvertSubToSuper() {
        // given
        final SubOneToOneA classA = new SubOneToOneA();
        classA.setIntSuper(10);

        // when
        final SuperOneToOneB classB = getMapper().map(classA, SuperOneToOneB.class);

        // then
        assertThat(classB.getIntSuper()).isEqualTo(10);
    }
}
