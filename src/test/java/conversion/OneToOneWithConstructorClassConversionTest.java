package conversion;

import static org.assertj.core.api.Assertions.assertThat;

import org.dozer.MappingException;
import org.junit.Test;

import base.DozerBaseTest;
import classes_a.OneToOneClassAWithConstructor;
import classes_b.OneToOneClassBWithConstructor;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class OneToOneWithConstructorClassConversionTest extends DozerBaseTest {

    @Test
    public void shouldConvertAToBUsingDirectFieldAccess() {
        // given
        final OneToOneClassAWithConstructor classA = new OneToOneClassAWithConstructor(10, "hello");

        // when
        final OneToOneClassBWithConstructor classB = getMapper().map(classA, OneToOneClassBWithConstructor.class);

        // then
        assertThat(classB.getIntA()).isEqualTo(10);
    }

    @Test(expected = MappingException.class)
    public void shouldThrowExceptionAsClassAIsNotAccessible() {
        // given
        final OneToOneClassBWithConstructor classB = new OneToOneClassBWithConstructor(10);

        // when
        getMapper().map(classB, OneToOneClassAWithConstructor.class);

        // then
    }
}
