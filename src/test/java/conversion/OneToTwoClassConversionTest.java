package conversion;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import base.DozerBaseTest;
import classes_a.OneToTwoClassA;
import classes_b.OneToTwoClassB;

/**
 * Created by ljozwiak on 08/05/2015.
 */
public class OneToTwoClassConversionTest extends DozerBaseTest {

    @Test
    public void shouldMapSingleFieldFromAToTwoFieldsFromB() {
        // given
        final OneToTwoClassA classA = new OneToTwoClassA();
        classA.setFirst(10);

        // when
        final OneToTwoClassB classB = getMapper().map(classA, OneToTwoClassB.class);

        // then
        assertThat(classB.getFirst()).isEqualTo(10);
        assertThat(classB.getSecond()).isEqualTo(10);

    }
}
